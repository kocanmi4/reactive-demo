package cz.kocanek.demo;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.data.annotation.Id;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class ServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}

	@Bean
	ApplicationListener<ApplicationReadyEvent> applicationReadyEventApplicationListener (VehicleRepository vehicleRepository) {
		return event -> Flux.just("Octavia", "Citroen", "Peugeot 207")
				.map(model -> new Vehicle(null, model))
				.flatMap(vehicleRepository::save)
				.subscribe(System.out::println);
	}

}

/**
 * Usually controller is separated in different package, but for better readability it is included here.
 */
@RestController
@RequiredArgsConstructor
class VehicleController {
	private final VehicleRepository vehicleRepository;

	@GetMapping("/vehicles")
	Flux<Vehicle> getAll() {
		return vehicleRepository.findAll();
	}

	@PostMapping("/vehicles")
	Mono<Vehicle> save(@RequestBody String model) {
		return vehicleRepository.save(new Vehicle(null, model));
	}
}
/**
 * Usually repository is separated in different package, but for better readability it is included here.
 */
interface VehicleRepository extends ReactiveCrudRepository<Vehicle, Integer> {}
/**
 * Usually model class is separated in different package, but for better readability it is included here.
 */
record Vehicle(@Id Integer id, String model) {}